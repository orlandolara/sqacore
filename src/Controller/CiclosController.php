<?php

namespace App\Controller;

use App\Entity\Ciclos;
use App\Form\CiclosType;
use App\Repository\CiclosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ciclos")
 */
class CiclosController extends AbstractController
{
    /**
     * @Route("/", name="ciclos_index", methods={"GET"})
     */
    public function index(CiclosRepository $ciclosRepository): Response
    {
        $ciclosTotales = $ciclosRepository->obtieneCiclosTotal();
        $proyectosCiclos = $ciclosRepository->obtieneProyectosDeCiclos();
        $ciclosFinalizados = $ciclosRepository->obtieneCiclosFinalizados();
        $ciclosEmpezados = $ciclosRepository->obtieneCiclosEmpezados();
        $duracionCiclos = $ciclosRepository->obtieneDuracionCiclos();

        return $this->render('ciclos/index.html.twig', [
            'ciclos' => $ciclosRepository->findAll(),
            'totales' => $ciclosTotales,
            'proyectos' => $proyectosCiclos,
            'finalizados' => $ciclosFinalizados,
            'empezados' => $ciclosEmpezados,
            'duracion' => $duracionCiclos,
        ]);
    }

    /**
     * @Route("/new", name="ciclos_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ciclo = new Ciclos();
        $form = $this->createForm(CiclosType::class, $ciclo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ciclo);
            $entityManager->flush();

            return $this->redirectToRoute('ciclos_index');
        }

        return $this->render('ciclos/new.html.twig', [
            'ciclo' => $ciclo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ciclos_show", methods={"GET"})
     */
    public function show(Ciclos $ciclo): Response
    {
        return $this->render('ciclos/show.html.twig', [
            'ciclo' => $ciclo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ciclos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ciclos $ciclo): Response
    {
        $form = $this->createForm(CiclosType::class, $ciclo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ciclos_index');
        }

        return $this->render('ciclos/edit.html.twig', [
            'ciclo' => $ciclo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ciclos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Ciclos $ciclo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ciclo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ciclo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ciclos_index');
    }
}
