<?php

namespace App\Controller;

use App\Entity\Defectos;
use App\Form\DefectosType;
use App\Repository\DefectosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/defectos")
 */
class DefectosController extends AbstractController
{
    /**
     * @Route("/", name="defectos_index", methods={"GET"})
     */
    public function index(DefectosRepository $defectosRepository): Response
    {

        $pendientes = $defectosRepository->obtienePendientes();
        $bajos = $defectosRepository->obtieneBajos();
        $medios = $defectosRepository->obtieneMedios();
        $altos = $defectosRepository->obtieneAltos();
        $urgentes = $defectosRepository->obtieneUrgentes();


        return $this->render('defectos/index.html.twig', [
            'defectos' => $defectosRepository->findAll(),
            'pendientes' => $pendientes,
            'bajos' => $bajos,
            'medios' => $medios,
            'altos' => $altos,
            'urgentes' => $urgentes,
        ]);
    }

    /**
     * @Route("/new", name="defectos_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $defecto = new Defectos();
        $form = $this->createForm(DefectosType::class, $defecto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($defecto);
            $entityManager->flush();

            return $this->redirectToRoute('defectos_index');
        }

        return $this->render('defectos/new.html.twig', [
            'defecto' => $defecto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="defectos_show", methods={"GET"})
     */
    public function show(Defectos $defecto): Response
    {
        return $this->render('defectos/show.html.twig', [
            'defecto' => $defecto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="defectos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Defectos $defecto): Response
    {
        $form = $this->createForm(DefectosType::class, $defecto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('defectos_index');
        }

        return $this->render('defectos/edit.html.twig', [
            'defecto' => $defecto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="defectos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Defectos $defecto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$defecto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($defecto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('defectos_index');
    }




}
