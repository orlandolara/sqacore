<?php

namespace App\Controller;

use App\Entity\Lenguage;
use App\Entity\Proyectos;
use App\Form\LenguageType;
use App\Repository\LenguageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lenguage")
 */
class LenguageController extends AbstractController
{
    /**
     * @Route("/", name="lenguage_index", methods={"GET"})
     */
    public function index(LenguageRepository $lenguageRepository): Response
    {
        return $this->render('lenguage/index.html.twig', [
            'lenguages' => $lenguageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="lenguage_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $lenguage = new Lenguage();
        $form = $this->createForm(LenguageType::class, $lenguage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lenguage);
            $entityManager->flush();

            return $this->redirectToRoute('lenguage_index');
        }

        return $this->render('lenguage/new.html.twig', [
            'lenguage' => $lenguage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lenguage_show", methods={"GET"})
     */
    public function show(Lenguage $lenguage): Response
    {
        return $this->render('lenguage/show.html.twig', [
            'lenguage' => $lenguage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lenguage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lenguage $lenguage): Response
    {
        $form = $this->createForm(LenguageType::class, $lenguage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lenguage_index');
        }

        return $this->render('lenguage/edit.html.twig', [
            'lenguage' => $lenguage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lenguage_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lenguage $lenguage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lenguage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lenguage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lenguage_index');
    }
}
