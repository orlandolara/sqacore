<?php

namespace App\Controller;

use App\Entity\Proyectos;
use App\Form\ProyectosType;
use App\Repository\ProyectosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/proyectos")
 */
class ProyectosController extends AbstractController
{
    /**
     * @Route("/", name="proyectos_index", methods={"GET"})
     */
    public function index(ProyectosRepository $proyectosRepository): Response
    {
        return $this->render('proyectos/index.html.twig', [
            'proyectos' => $proyectosRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="proyectos_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $proyecto = new Proyectos();
        $form = $this->createForm(ProyectosType::class, $proyecto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($proyecto);
            $entityManager->flush();

            return $this->redirectToRoute('proyectos_index');
        }

        return $this->render('proyectos/new.html.twig', [
            'proyecto' => $proyecto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="proyectos_show", methods={"GET"})
     */
    public function show(Proyectos $proyecto): Response
    {
        return $this->render('proyectos/show.html.twig', [
            'proyecto' => $proyecto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="proyectos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Proyectos $proyecto): Response
    {
        $form = $this->createForm(ProyectosType::class, $proyecto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('proyectos_index');
        }

        return $this->render('proyectos/edit.html.twig', [
            'proyecto' => $proyecto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="proyectos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Proyectos $proyecto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$proyecto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($proyecto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('proyectos_index');
    }
}
