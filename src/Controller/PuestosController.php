<?php

namespace App\Controller;

use App\Entity\Puestos;
use App\Form\PuestosType;
use App\Repository\PuestosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/puestos")
 */
class PuestosController extends AbstractController
{
    /**
     * @Route("/", name="puestos_index", methods={"GET"})
     */
    public function index(PuestosRepository $puestosRepository): Response
    {
        return $this->render('puestos/index.html.twig', [
            'puestos' => $puestosRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="puestos_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $puesto = new Puestos();
        $form = $this->createForm(PuestosType::class, $puesto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($puesto);
            $entityManager->flush();

            return $this->redirectToRoute('puestos_index');
        }

        return $this->render('puestos/new.html.twig', [
            'puesto' => $puesto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="puestos_show", methods={"GET"})
     */
    public function show(Puestos $puesto): Response
    {
        return $this->render('puestos/show.html.twig', [
            'puesto' => $puesto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="puestos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Puestos $puesto): Response
    {
        $form = $this->createForm(PuestosType::class, $puesto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('puestos_index');
        }

        return $this->render('puestos/edit.html.twig', [
            'puesto' => $puesto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="puestos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Puestos $puesto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$puesto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($puesto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('puestos_index');
    }
}
