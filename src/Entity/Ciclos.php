<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CiclosRepository")
 */
class Ciclos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_ciclo;

    /**
     * @ORM\Column(type="date")
     */
    private $f_ini_prog;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $f_ini_real;

    /**
     * @ORM\Column(type="date")
     */
    private $f_fin_prog;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $f_fin_real;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proyectos", inversedBy="ciclos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proyecto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Defectos", mappedBy="ciclo")
     */
    private $defectos;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $estatusCiclo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duracionCiclos;


    public function __construct()
    {
        $this->defectos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreCiclo(): ?string
    {
        return $this->nombre_ciclo;
    }

    public function setNombreCiclo(string $nombre_ciclo): self
    {
        $this->nombre_ciclo = $nombre_ciclo;

        return $this;
    }

    public function getFIniProg(): ?\DateTimeInterface
    {
        return $this->f_ini_prog;
    }

    public function setFIniProg(\DateTimeInterface $f_ini_prog): self
    {
        $this->f_ini_prog = $f_ini_prog;

        return $this;
    }

    public function getFIniReal(): ?\DateTimeInterface
    {
        return $this->f_ini_real;
    }

    public function setFIniReal(?\DateTimeInterface $f_ini_real): self
    {
        $this->f_ini_real = $f_ini_real;

        return $this;
    }

    public function getFFinProg(): ?\DateTimeInterface
    {
        return $this->f_fin_prog;
    }

    public function setFFinProg(\DateTimeInterface $f_fin_prog): self
    {
        $this->f_fin_prog = $f_fin_prog;

        return $this;
    }

    public function getFFinReal(): ?\DateTimeInterface
    {
        return $this->f_fin_real;
    }

    public function setFFinReal(?\DateTimeInterface $f_fin_real): self
    {
        $this->f_fin_real = $f_fin_real;

        return $this;
    }

    public function getProyecto(): ?Proyectos
    {
        return $this->proyecto;
    }

    public function setProyecto(?Proyectos $proyecto): self
    {
        $this->proyecto = $proyecto;

        return $this;
    }

    /**
     * @return Collection|Defectos[]
     */
    public function getDefectos(): Collection
    {
        return $this->defectos;
    }

    public function addDefecto(Defectos $defecto): self
    {
        if (!$this->defectos->contains($defecto)) {
            $this->defectos[] = $defecto;
            $defecto->setCiclo($this);
        }

        return $this;
    }

    public function removeDefecto(Defectos $defecto): self
    {
        if ($this->defectos->contains($defecto)) {
            $this->defectos->removeElement($defecto);
            // set the owning side to null (unless already changed)
            if ($defecto->getCiclo() === $this) {
                $defecto->setCiclo(null);
            }
        }

        return $this;
    }

    public function getEstatusCiclo(): ?string
    {
        return $this->estatusCiclo;
    }

    public function setEstatusCiclo(?string $estatusCiclo): self
    {
        $this->estatusCiclo = $estatusCiclo;

        return $this;
    }

    public function getDuracionCiclos(): ?int
    {
        return $this->duracionCiclos;
    }

    public function setDuracionCiclos(?int $duracionCiclos): self
    {
        $this->duracionCiclos = $duracionCiclos;

        return $this;
    }

}
