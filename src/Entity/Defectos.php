<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DefectosRepository")
 */
class Defectos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $asunto_defecto;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $descripcion_defecto;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_reportado;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $estatus;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nivel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ciclos", inversedBy="defectos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ciclo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuarios", inversedBy="defectos")
     * @ORM\JoinColumn(nullable=true)
     */
    private $usuario;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $tester;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proyectos", inversedBy="defectos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proyecto;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tipo_defecto;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_corregido;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $evidencia;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAsuntoDefecto(): ?string
    {
        return $this->asunto_defecto;
    }

    public function setAsuntoDefecto(string $asunto_defecto): self
    {
        $this->asunto_defecto = $asunto_defecto;

        return $this;
    }

    public function getDescripcionDefecto(): ?string
    {
        return $this->descripcion_defecto;
    }

    public function setDescripcionDefecto(?string $descripcion_defecto): self
    {
        $this->descripcion_defecto = $descripcion_defecto;

        return $this;
    }

    public function getFechaReportado(): ?\DateTimeInterface
    {
        return $this->fecha_reportado;
    }

    public function setFechaReportado(\DateTimeInterface $fecha_reportado): self
    {
        $this->fecha_reportado = $fecha_reportado;

        return $this;
    }

    public function getEstatus(): ?string
    {
        return $this->estatus;
    }

    public function setEstatus(string $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getNivel(): ?string
    {
        return $this->nivel;
    }

    public function setNivel(string $nivel): self
    {
        $this->nivel = $nivel;

        return $this;
    }

    public function getCiclo(): ?Ciclos
    {
        return $this->ciclo;
    }

    public function setCiclo(?Ciclos $ciclo): self
    {
        $this->ciclo = $ciclo;

        return $this;
    }

    public function getUsuario(): ?Usuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getTester(): ?string
    {
        return $this->tester;
    }

    public function setTester(string $tester): self
    {
        $this->tester = $tester;

        return $this;
    }

    public function getProyecto(): ?Proyectos
    {
        return $this->proyecto;
    }

    public function setProyecto(?Proyectos $proyecto): self
    {
        $this->proyecto = $proyecto;

        return $this;
    }

    public function getTipoDefecto(): ?string
    {
        return $this->tipo_defecto;
    }

    public function setTipoDefecto(string $tipo_defecto): self
    {
        $this->tipo_defecto = $tipo_defecto;

        return $this;
    }

    public function getFechaCorregido(): ?\DateTimeInterface
    {
        return $this->fecha_corregido;
    }

    public function setFechaCorregido(?\DateTimeInterface $fecha_corregido): self
    {
        $this->fecha_corregido = $fecha_corregido;

        return $this;
    }

    public function getEvidencia(): ?string
    {
        return $this->evidencia;
    }

    public function setEvidencia(?string $evidencia): self
    {
        $this->evidencia = $evidencia;

        return $this;
    }
}
