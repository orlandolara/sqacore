<?php
namespace App\Entity;

use App\Entity\Proyectos;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LenguageRepository")
 */
class Lenguage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_lenguage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icono;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proyectos", mappedBy="lenguage")
     */
    private $proyectos;

    public function __construct()
    {
        $this->proyectos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreLenguage(): ?string
    {
        return $this->nombre_lenguage;
    }

    public function setNombreLenguage(string $nombre_lenguage): self
    {
        $this->nombre_lenguage = $nombre_lenguage;

        return $this;
    }
    
    public function getIcono(): ?string
    {
        return $this->icono;
    }

    public function setIcono(?string $icono): self
    {
        $this->icono = $icono;

        return $this;
    }

    /**
     * @return Collection|Proyectos[]
     */
    public function getProyectos(): Collection
    {
        return $this->proyectos;
    }

    public function addProyecto(Proyectos $proyecto): self
    {
        if (!$this->proyectos->contains($proyecto)) {
            $this->proyectos[] = $proyecto;
            $proyecto->setLenguage($this);
        }

        return $this;
    }

    public function removeProyecto(Proyectos $proyecto): self
    {
        if ($this->proyectos->contains($proyecto)) {
            $this->proyectos->removeElement($proyecto);
            // set the owning side to null (unless already changed)
            if ($proyecto->getLenguage() === $this) {
                $proyecto->setLenguage(null);
            }
        }

        return $this;
    }
}
