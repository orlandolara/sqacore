<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProyectosRepository")
 */
class Proyectos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $clave_proyecto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_proyecto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuarios", inversedBy="proyectos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $responsable;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ciclos", mappedBy="proyecto")
     */
    private $ciclos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Defectos", mappedBy="proyecto")
     */
    private $defectos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lenguage", inversedBy="proyectos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lenguage;

    public function __construct()
    {
        $this->ciclos = new ArrayCollection();
        $this->defectos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClaveProyecto(): ?string
    {
        return $this->clave_proyecto;
    }

    public function setClaveProyecto(string $clave_proyecto): self
    {
        $this->clave_proyecto = $clave_proyecto;

        return $this;
    }

    public function getNombreProyecto(): ?string
    {
        return $this->nombre_proyecto;
    }

    public function setNombreProyecto(string $nombre_proyecto): self
    {
        $this->nombre_proyecto = $nombre_proyecto;

        return $this;
    }

    public function getResponsable(): ?Usuarios
    {
        return $this->responsable;
    }

    public function setResponsable(?Usuarios $responsable): self
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * @return Collection|Ciclos[]
     */
    public function getCiclos(): Collection
    {
        return $this->ciclos;
    }

    public function addCiclo(Ciclos $ciclo): self
    {
        if (!$this->ciclos->contains($ciclo)) {
            $this->ciclos[] = $ciclo;
            $ciclo->setProyecto($this);
        }

        return $this;
    }

    public function removeCiclo(Ciclos $ciclo): self
    {
        if ($this->ciclos->contains($ciclo)) {
            $this->ciclos->removeElement($ciclo);
            // set the owning side to null (unless already changed)
            if ($ciclo->getProyecto() === $this) {
                $ciclo->setProyecto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Defectos[]
     */
    public function getDefectos(): Collection
    {
        return $this->defectos;
    }

    public function addDefecto(Defectos $defecto): self
    {
        if (!$this->defectos->contains($defecto)) {
            $this->defectos[] = $defecto;
            $defecto->setProyecto($this);
        }

        return $this;
    }

    public function removeDefecto(Defectos $defecto): self
    {
        if ($this->defectos->contains($defecto)) {
            $this->defectos->removeElement($defecto);
            // set the owning side to null (unless already changed)
            if ($defecto->getProyecto() === $this) {
                $defecto->setProyecto(null);
            }
        }

        return $this;
    }

    public function getLenguage(): ?Lenguage
    {
        return $this->lenguage;
    }

    public function setLenguage(?Lenguage $lenguage): self
    {
        $this->lenguage = $lenguage;

        return $this;
    }
}
