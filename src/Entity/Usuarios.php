<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuariosRepository")
 */
class Usuarios
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_usuario;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $email_usuario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Puestos", inversedBy="usuarios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $puesto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proyectos", mappedBy="responsable")
     */
    private $proyectos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Defectos", mappedBy="usuario")
     */
    private $defectos;

    public function __construct()
    {
        $this->proyectos = new ArrayCollection();
        $this->defectos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreUsuario(): ?string
    {
        return $this->nombre_usuario;
    }

    public function setNombreUsuario(string $nombre_usuario): self
    {
        $this->nombre_usuario = $nombre_usuario;

        return $this;
    }

    public function getEmailUsuario(): ?string
    {
        return $this->email_usuario;
    }

    public function setEmailUsuario(string $email_usuario): self
    {
        $this->email_usuario = $email_usuario;

        return $this;
    }

    public function getPuesto(): ?Puestos
    {
        return $this->puesto;
    }

    public function setPuesto(?Puestos $puesto): self
    {
        $this->puesto = $puesto;

        return $this;
    }

    /**
     * @return Collection|Proyectos[]
     */
    public function getProyectos(): Collection
    {
        return $this->proyectos;
    }

    public function addProyecto(Proyectos $proyecto): self
    {
        if (!$this->proyectos->contains($proyecto)) {
            $this->proyectos[] = $proyecto;
            $proyecto->setResponsable($this);
        }

        return $this;
    }

    public function removeProyecto(Proyectos $proyecto): self
    {
        if ($this->proyectos->contains($proyecto)) {
            $this->proyectos->removeElement($proyecto);
            // set the owning side to null (unless already changed)
            if ($proyecto->getResponsable() === $this) {
                $proyecto->setResponsable(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Defectos[]
     */
    public function getDefectos(): Collection
    {
        return $this->defectos;
    }

    public function addDefecto(Defectos $defecto): self
    {
        if (!$this->defectos->contains($defecto)) {
            $this->defectos[] = $defecto;
            $defecto->setUsuario($this);
        }

        return $this;
    }

    public function removeDefecto(Defectos $defecto): self
    {
        if ($this->defectos->contains($defecto)) {
            $this->defectos->removeElement($defecto);
            // set the owning side to null (unless already changed)
            if ($defecto->getUsuario() === $this) {
                $defecto->setUsuario(null);
            }
        }

        return $this;
    }
}
