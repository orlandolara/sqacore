<?php

namespace App\Form;

use App\Entity\Ciclos;
use App\Entity\Proyectos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Form\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CiclosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre_ciclo')
        ->add('proyecto', EntityType::class, [
            'class' => Proyectos::class,
            'choice_label' => 'nombreProyecto', 
            'attr' => ['class' => 'claserow1'],             
        ])
        ->add('f_ini_prog', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker claserow1 input-group-prepend'],
                
            ])
        ->add('f_ini_real', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker claserow1 input-group-prepend'],
                
            ])
        ->add('f_fin_prog', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker claserow1 input-group-prepend'],
                
            ])
        ->add('f_fin_real', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker claserow1 input-group-prepend'],
                
            ])

        ->add('estatusCiclo', ChoiceType::class, [
            'choices' => [
                'Seleccione el estatus del request' => null,
                'Sin iniciar' => 'Sin iniciar',
                'Iniciado' => 'Iniciado',
                'Cancelado' => 'Cancelado',
                'Finalizado' => 'Finalizado'
            ],
                'attr' => ['class' => 'claserow1'],

        ])
        ->add('duracionCiclos', null, [
                'attr' => ['class' => 'example', 'readonly' => 'readonly', 'value' => '0'], 
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ciclos::class,
        ]);
    }
}
