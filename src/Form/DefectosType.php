<?php

namespace App\Form;

use App\Entity\Defectos;
use App\Entity\Ciclos;
use App\Entity\Usuarios;
use App\Entity\Proyectos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Form\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DefectosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('tipo_defecto', ChoiceType::class, [
            'choices' => [
                '¿Qué incidencia quier crear?' => null,
                'Un Bug' => 'Bug',
                'Una Sugerencia' => 'Sugerencia'
            ],
                'attr' => ['class' => 'claserow1'],

        ])
            ->add('asunto_defecto')
            ->add('evidencia')
            ->add('descripcion_defecto')
            ->add('fecha_reportado', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker claserow1 input-group-prepend'],
                
            ])
            ->add('fecha_corregido', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker claserow1 input-group-prepend', 'disabled' => 'disabled'],
                
            ])
            ->add('estatus', ChoiceType::class, [
                'choices' => [
                    '¿En qué estatus se encuentra la incidencia?' => null,
                    'Pendiente' => 'Pendiente',
                    'Terminado' => 'Terminado'
                ],
                   'attr' => ['class' => 'claserow1'],
            ])
            ->add('nivel', ChoiceType::class, [
                'choices' => [
                    'Seleccione el nivel de gravedad' => null,
                    'Bajo' => 'Bajo',
                    'Medio' => 'Medio',
                    'Alto' => 'Alto',
                    'Urgente' => 'Urgente'
                ],
                    'attr' => ['class' => 'claserow1'],
            ])
            ->add('tester', ChoiceType::class, [
                'choices' => [
                    'Seleccione un informante' => null,
                    'Orlando Lara' => 'Orlando Lara',
                    'Gerardo Kantun' => 'Gerardo Kantun'
                ],
                   'attr' => ['class' => 'claserow1'],
            ])
            ->add('ciclo', EntityType::class, [
                'class' => Ciclos::class,
                'choice_label' => 'nombreCiclo', 
                   'attr' => ['class' => 'claserow1'],             
        ])
            ->add('usuario', EntityType::class, [
                'class' => Usuarios::class,
                'choice_label' => 'nombreUsuario', 
                   'attr' => ['class' => 'claserow1', 'disabled' => 'disabled'],             
        ])
        ->add('proyecto', EntityType::class, [
            'class' => Proyectos::class,
            'choice_label' => 'nombreProyecto', 
               'attr' => ['class' => 'claserow1'],             
    ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Defectos::class,
        ]);
    }
}
