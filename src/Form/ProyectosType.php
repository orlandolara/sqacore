<?php

namespace App\Form;

use App\Entity\Proyectos;
use App\Entity\Lenguage;
use App\Entity\Usuarios;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Form\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProyectosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave_proyecto')
            ->add('nombre_proyecto')
            ->add('lenguage', EntityType::class, [
                'class' => Lenguage::class,
                'choice_label' => 'nombreLenguage', 
                   'attr' => ['class' => 'claserow1'],             
        ])

            ->add('responsable', EntityType::class, [
                'class' => Usuarios::class,
                'choice_label' => 'nombreUsuario', 
                   'attr' => ['class' => 'claserow1'],             
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Proyectos::class,
        ]);
    }
}
