<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106173934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ciclos (id INT AUTO_INCREMENT NOT NULL, proyecto_id INT NOT NULL, nombre_ciclo VARCHAR(255) NOT NULL, f_ini_prog DATE NOT NULL, f_ini_real DATE DEFAULT NULL, f_fin_prog DATE NOT NULL, f_fin_real DATE DEFAULT NULL, INDEX IDX_E8B7F42CF625D1BA (proyecto_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE defectos (id INT AUTO_INCREMENT NOT NULL, ciclo_id INT NOT NULL, usuario_id INT NOT NULL, asunto_defecto VARCHAR(200) NOT NULL, descripcion_defecto VARCHAR(400) DEFAULT NULL, fecha_reportado DATE NOT NULL, estatus VARCHAR(120) NOT NULL, nivel VARCHAR(100) NOT NULL, tester VARCHAR(150) NOT NULL, INDEX IDX_3670C99FD8F6DC8 (ciclo_id), INDEX IDX_3670C99FDB38439E (usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lenguage (id INT AUTO_INCREMENT NOT NULL, nombre_lenguage VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proyectos (id INT AUTO_INCREMENT NOT NULL, responsable_id INT NOT NULL, clave_proyecto VARCHAR(5) NOT NULL, nombre_proyecto VARCHAR(255) NOT NULL, INDEX IDX_A9DC162153C59D72 (responsable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proyectos_lenguage (proyectos_id INT NOT NULL, lenguage_id INT NOT NULL, INDEX IDX_737A979DCC33C266 (proyectos_id), INDEX IDX_737A979D8B1A1A8E (lenguage_id), PRIMARY KEY(proyectos_id, lenguage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE puestos (id INT AUTO_INCREMENT NOT NULL, nombre_puesto VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuarios (id INT AUTO_INCREMENT NOT NULL, puesto_id INT NOT NULL, nombre_usuario VARCHAR(255) NOT NULL, email_usuario VARCHAR(150) NOT NULL, INDEX IDX_EF687F25035E9DA (puesto_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ciclos ADD CONSTRAINT FK_E8B7F42CF625D1BA FOREIGN KEY (proyecto_id) REFERENCES proyectos (id)');
        $this->addSql('ALTER TABLE defectos ADD CONSTRAINT FK_3670C99FD8F6DC8 FOREIGN KEY (ciclo_id) REFERENCES ciclos (id)');
        $this->addSql('ALTER TABLE defectos ADD CONSTRAINT FK_3670C99FDB38439E FOREIGN KEY (usuario_id) REFERENCES usuarios (id)');
        $this->addSql('ALTER TABLE proyectos ADD CONSTRAINT FK_A9DC162153C59D72 FOREIGN KEY (responsable_id) REFERENCES usuarios (id)');
        $this->addSql('ALTER TABLE proyectos_lenguage ADD CONSTRAINT FK_737A979DCC33C266 FOREIGN KEY (proyectos_id) REFERENCES proyectos (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proyectos_lenguage ADD CONSTRAINT FK_737A979D8B1A1A8E FOREIGN KEY (lenguage_id) REFERENCES lenguage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F25035E9DA FOREIGN KEY (puesto_id) REFERENCES puestos (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE defectos DROP FOREIGN KEY FK_3670C99FD8F6DC8');
        $this->addSql('ALTER TABLE proyectos_lenguage DROP FOREIGN KEY FK_737A979D8B1A1A8E');
        $this->addSql('ALTER TABLE ciclos DROP FOREIGN KEY FK_E8B7F42CF625D1BA');
        $this->addSql('ALTER TABLE proyectos_lenguage DROP FOREIGN KEY FK_737A979DCC33C266');
        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F25035E9DA');
        $this->addSql('ALTER TABLE defectos DROP FOREIGN KEY FK_3670C99FDB38439E');
        $this->addSql('ALTER TABLE proyectos DROP FOREIGN KEY FK_A9DC162153C59D72');
        $this->addSql('DROP TABLE ciclos');
        $this->addSql('DROP TABLE defectos');
        $this->addSql('DROP TABLE lenguage');
        $this->addSql('DROP TABLE proyectos');
        $this->addSql('DROP TABLE proyectos_lenguage');
        $this->addSql('DROP TABLE puestos');
        $this->addSql('DROP TABLE usuarios');
    }
}
