<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106192843 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE proyectos_lenguage');
        $this->addSql('ALTER TABLE proyectos ADD lenguage_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proyectos ADD CONSTRAINT FK_A9DC16218B1A1A8E FOREIGN KEY (lenguage_id) REFERENCES lenguage (id)');
        $this->addSql('CREATE INDEX IDX_A9DC16218B1A1A8E ON proyectos (lenguage_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proyectos_lenguage (proyectos_id INT NOT NULL, lenguage_id INT NOT NULL, INDEX IDX_737A979D8B1A1A8E (lenguage_id), INDEX IDX_737A979DCC33C266 (proyectos_id), PRIMARY KEY(proyectos_id, lenguage_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE proyectos_lenguage ADD CONSTRAINT FK_737A979D8B1A1A8E FOREIGN KEY (lenguage_id) REFERENCES lenguage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proyectos_lenguage ADD CONSTRAINT FK_737A979DCC33C266 FOREIGN KEY (proyectos_id) REFERENCES proyectos (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proyectos DROP FOREIGN KEY FK_A9DC16218B1A1A8E');
        $this->addSql('DROP INDEX IDX_A9DC16218B1A1A8E ON proyectos');
        $this->addSql('ALTER TABLE proyectos DROP lenguage_id');
    }
}
