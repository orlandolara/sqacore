<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106220936 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE defectos ADD proyecto_id INT NOT NULL');
        $this->addSql('ALTER TABLE defectos ADD CONSTRAINT FK_3670C99FF625D1BA FOREIGN KEY (proyecto_id) REFERENCES proyectos (id)');
        $this->addSql('CREATE INDEX IDX_3670C99FF625D1BA ON defectos (proyecto_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE defectos DROP FOREIGN KEY FK_3670C99FF625D1BA');
        $this->addSql('DROP INDEX IDX_3670C99FF625D1BA ON defectos');
        $this->addSql('ALTER TABLE defectos DROP proyecto_id');
    }
}
