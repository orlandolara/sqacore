<?php

namespace App\Repository;

use App\Entity\Ciclos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ciclos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ciclos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ciclos[]    findAll()
 * @method Ciclos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CiclosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ciclos::class);
    }


        public function obtieneCiclosTotal()
    {
       

        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }



        public function obtieneProyectosDeCiclos()
    {
       

        return $this->createQueryBuilder('c')
            ->select('COUNT(DISTINCT c.proyecto)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

      public function obtieneCiclosFinalizados()
    {
       

        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->andWhere('c.f_fin_real != :val')
            ->setParameter('val', '')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

        public function obtieneCiclosEmpezados()
    {
       

        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->andWhere('c.f_fin_real is NULL')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }


          public function obtieneDuracionCiclos()
    {
       

        return $this->createQueryBuilder('c')
            ->select('DATE_DIFF(c.f_fin_prog, c.f_ini_prog)')
            ->getQuery()
            ->getScalarResult()
        ;
    }



          public function paraGraficarCiclos()
    {
       

        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id), MONTH(c.f_fin_real), YEAR(c.f_fin_real)')
            ->andWhere('c.estatus_ciclo =  "Finalizado" GROUP BY MONTH(c.f_fin_real)')
            ->getQuery()
            ->getScalarResult()
        ;
    }


    // /**
    //  * @return Ciclos[] Returns an array of Ciclos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ciclos
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
