<?php

namespace App\Repository;

use App\Entity\Defectos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Defectos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Defectos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Defectos[]    findAll()
 * @method Defectos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefectosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Defectos::class);
    }

    // /**
    //  * @return Defectos[] Returns an array of Defectos objects
    //  */

    /*
    public function obtieneTotales()
    {
        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    } */

     public function obtienePendientes()
    {
       

        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->andWhere('d.estatus = :val')
            ->setParameter('val', 'Pendiente')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

      public function obtieneBajos()
    {
       

        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->andWhere('d.nivel = :val')
            ->setParameter('val', 'Bajo')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

      public function obtieneMedios()
    {
       

        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->andWhere('d.nivel = :val')
            ->setParameter('val', 'Medio')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

      public function obtieneAltos()
    {
       

        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->andWhere('d.nivel = :val')
            ->setParameter('val', 'Alto')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

      public function obtieneUrgentes()
    {
       

        return $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->andWhere('d.nivel = :val')
            ->setParameter('val', 'Urgente')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }





    /*
    public function findOneBySomeField($value): ?Defectos
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
