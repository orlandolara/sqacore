<?php

namespace App\Repository;

use App\Entity\Lenguage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lenguage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lenguage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lenguage[]    findAll()
 * @method Lenguage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LenguageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lenguage::class);
    }

    // /**
    //  * @return Lenguage[] Returns an array of Lenguage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lenguage
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
